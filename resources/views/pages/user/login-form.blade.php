<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-12 col-xs-12">
        <form action="{{ url('login') }}" class="form" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="email" name="email" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" name="password" placeholder="Password" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" value="Login" class="btn btn-default form-control">
            </div>
        </form>
    </div>
</div>