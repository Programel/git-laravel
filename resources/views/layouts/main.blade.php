<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>{{ config('app.name') }}</title>
</head>
<body>

<div class="navbar navbar-default custom-navbar">
    <div class="nav-left">
        <a href="{{ url('/') }}">{{ config('app.name') }}</a>
    </div>
    <div class="nav-right">
        @include('layouts.main-navigation')
    </div>
</div>
<div class="container">
    @yield('content')
</div>

</body>
</html>