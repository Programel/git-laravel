<ul class="ul-main-nav">
    <li class="li-main-nav">
        <a href="{{ url('/') }}">home</a>
    </li>
    <li class="li-main-nav">
        <a href="{{ url('about') }}">about</a>
    </li>
    <li class="li-main-nav">
        <a href="{{ url('contacts') }}">contacts</a>
    </li>
    <li class="li-main-nav">
        <a href="{{ url('login') }}">login</a>
    </li>
</ul>