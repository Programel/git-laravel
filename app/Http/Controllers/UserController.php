<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator as ValidatorService;

class UserController extends Controller
{

    public function loginStore(Request $request, ValidatorService $vs)
    {
        $validator = $vs::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        return response()->json($request->all());
    }

    public function loginView()
    {
        return view('pages.user.login');
    }

    public function registerView()
    {
        return view('pages.user.register');
    }
}
